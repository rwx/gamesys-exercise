import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface User {
  name: string;
  email: string;
  balance: number;
  address: string;
  about: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users: Observable<User[]>;

  constructor(private http: HttpClient) {
    this.users = this.getUsers();
  }

  getUsers() {
    return this.http.get<User[]>('/assets/users.json');
  }
}
